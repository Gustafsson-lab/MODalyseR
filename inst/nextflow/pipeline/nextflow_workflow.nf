#!/usr/bin/env nextflow
 
 run_script = file(params.run_script)
 r_env = file(params.env)
 result_dir = file(params.result_dir)
 process run_inference {

  """
  Rscript $run_script $r_env $result_dir
  """
 }