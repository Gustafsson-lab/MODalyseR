#' tigress UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd 
#'
#' @importFrom shiny NS tagList 
mod_tigress_ui <- function(id){
  ns <- NS(id)
  tagList(
    uiOutput(ns("input_choice")),
    tags$a(class="collapsible", "Advanced settings", class = "btn btn-primary btn-block", "data-toggle" = 'collapse', "data-target" = '#tigress_adv',"aria-expanded" = 'false', "style" = 'font-size:16px;', 
           tags$div(class= "expand_caret caret")),
    tags$div(id = "error_name_tigress_js",
             tags$div(id = "tigress_adv", class = "collapse",
                      shiny::sliderInput(ns("edge_cutoff"), label = "Maximum number of interactions to infer", min = 10000, max = 500000, value = 100000, step = 10000),
                      #textInput(ns("edge_cutoff"), "Number of interactions to infer", value="100000", popup = "The maximum number of interactions that should be inferred"),
                      shiny::sliderInput(ns("nstepsLARS"), label = "Number of LARS steps to perform in stability selection", min = 1, max = 10, value = 5),
                      #textInput(ns("nstepsLARS"), "nstepsLARS", value=5, popup = "Number of LARS steps to perform in stability selection"),
                      shiny::sliderInput(ns("alpha"), label = "Randomization level in stability selection. alpha=1 no randomisation", min = 0, max = 1, value = .2),
                      #textInput(ns("alpha"), "alpha", value=0.2, popup = "Randomization level in stability selection. Should be between 0 and 1. If alpha=1, no randomisation is used"),
                      shiny::sliderInput(ns("nsplit"), label = "Number of sample splits to perform in stability selection", min = 0, max = 1, value = .2))
                      #textInput(ns("nsplit"), "nsplit", value=100, popup = "Number of sample splits to perform in stability selection"))
    ),
    
    uiOutput(ns("error_name_descrip")),
    uiOutput(ns("error_name_js")),
    tags$br(),
    tags$div(style = "text-align:center",
             actionButton(ns("load_input"), "Infer network", onclick="loading_modal_open(); stopWatch()"),
             htmlOutput(ns("close_loading_modal")),  # Close modal with JS
             downloadButton(ns("add_nextflow"), "Download nextflow script"),
             tags$br(),
             downloadButton(ns("download_network"), 'Download inferred network')
    )
  )
}

#' DiffCoEx Server Function
#'
#' @noRd 
mod_tigress_server <- function(input, output, session, con, init_comhub_ui_1, comhub_overview_ui_1){
  ns <- session$ns
  
  tigress_module <- reactiveValues()
  x <- reactiveVal(1)  # Reactive value to record if the input button is pressed
  
  output$input_choice <- renderUI({
    input_objects <- unlist(MODifieRDB::get_available_comhub_objects(con)$comhub_name)
    selectInput(ns("input_object"), label = "ComHub object", choices = input_objects, popup = "The input used for analyzation")
  })
  
  observeEvent(c(init_comhub_ui_1$comhub_name), {#, comhub_overview_ui_1$value$delete, comhub_overview_ui_1$value$upload), {
    input_objects <- unlist(MODifieRDB::get_available_comhub_objects(con)$comhub_name)
    updateSelectInput(session, "input_object", choices = input_objects)
  })
  
  edge_cutoff <- reactive({
    input$edge_cutoff
  })
  
  nstepsLARS <- reactive({
    input$nstepsLARS
  })
  
  alpha <- reactive({
    input$alpha
  })
  
  nsplit <- reactive({
    input$nsplit
  })
  
  observeEvent(input$load_input, {
    id <- showNotification("Infering network", duration = NULL, closeButton = FALSE, type = "warning")
    on.exit(removeNotification(id), add = TRUE)
    
    comhub_object <- try(MODifieRDB::tigress_comhub_db(comhub_name = input$input_object,
                                                       network_cutoff = as.integer(input$edge_cutoff),
                                                       nstepsLARS = as.integer(input$nstepsLARS),
                                                       alpha = as.numeric(input$alpha),
                                                       nsplit = as.integer(input$nsplit),
                                                       con = con))
    
    if (class(comhub_object) == "try-error"){
      output$error_name_descrip <- renderUI({
        tags$p(class = "text-danger", tags$b("Error:"), comhub_object,
               style = "-webkit-animation: fadein 0.5s; -moz-animation: fadein 0.5s; -ms-animation: fadein 0.5s;-o-animation: fadein 0.5s; animation: fadein 0.5s;")
      })
    } else {
      x(x() + 1)
      tigress_module$infer_net <- x()
      updateTextInput(session, "edge_cutoff", value = "100000") #character(0))
      updateTextInput(session, "nstepsLARS", value = 5)
      updateTextInput(session, "alpha", value = 0.2)
      updateTextInput(session, "nsplit", value = 100)
    }
    output$close_loading_modal <- renderUI({
      tags$script("loading_modal_close(); reset();")
    })
  })
  
  retrieve_network_object <- function(){
    comhub_object <- MODifieRDB::comhub_object_from_db(input$input_object, con = con)
    data <- comhub_object$results$tigress
    return(data)
  }
  
  # Download function
  output$download_network<- downloadHandler(
    filename = function() {
      paste0("tigress_network_", input$input_object, '_', Sys.Date(), ".tsv", sep="")
    },
    content = function(file) {
      write.table(retrieve_network_object(), file, quote = F, row.names = F, sep = '\t')
    }
  )
  output$add_nextflow <- downloadHandler(
    filename = function() {
      paste0("nextflow_pipeline", Sys.Date(), ".zip", sep="")
    },
    content = function(file) {
      module_name <- input$input_object
      inference_method<- "tigress_comhub"
      arguments <- tigress_nf(comhub_name = input$input_object, 
                              network_cutoff = as.integer(input$edge_cutoff),
                              nstepsLARS = input$nstepsLARS,
                              alpha = input$alpha,
                              nsplit = input$nsplit, 
                              con = con)
      tempd <- tempdir()
      file.copy(find_nextflow(), tempd, recursive = T)
      zipfile <- paste0(tempd, "/nextflow/data/env.RData")
      save(arguments, inference_method, module_name, file = zipfile)
      zip::zipr(file, files = list.dirs(paste0(tempd, "/nextflow"), recursive = F), mode = "cherry-pick")
    },
    contentType = "application/zip"
  )
  return(tigress_module)
}

## To be copied in the UI
# mod_tigress_ui("tigress_ui_1")

## To be copied in the server
# callModule(mod_tigress_server, "tigress_ui_1")

