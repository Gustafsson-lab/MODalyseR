#' welcoming_page UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#'
#' @noRd 
#'
#' @importFrom shiny NS tagList 
#' 

tool_button <- '<button id = "tool_button" class ="front_button">
View app
</button>'

user_guide_button <- '<button id = "user_guide_btn" class = "front_button" style = "position:absolute; top:80%">
  User guide
</button>'

mod_welcoming_page_ui <- function(id){
  ns <- NS(id)
  tagList(
    # section 1
    tags$div(style="height: 100vh; width: 100%; background-image: url('www/front_page1.gif');background-repeat:no-repeat;background-size:cover; color:bafa19", class= "row",
             tags$div(`class`="container", style = "margin: 17vh 10vw; padding: 0",
                      tags$h1("MODalyseR", style = "margin: 0; font-family:Quicksand; color:bafa19; font-size:60px", color = "bafa19"),
                      rep_br(2)),
             HTML(tool_button, user_guide_button),#, tutorial_button),
             HTML('<a class="button learn" href="#section2">&#8595;</a>'),
             tags$a(icon("gitlab", "fa-3x", lib = "font-awesome"), 
                    href = "https://gitlab.com/Gustafsson-lab/MODalyseR",
                    target = "_blank",
                    style = "position: absolute;
                             bottom: 5vh;
                             left: 5vh;")),
    ## section 3
    tags$div(id="section3", style="height: 100vh; width: 100%;text-align:center; background-color:#0C090A", class= "row",
             tags$div(`class`="col-sm-4", style="height: 100%;text-align: center;padding-left: 3%;",
                       tags$h3(style ="color:white; margin: 0;position: absolute; top: 50%;-ms-transform: translateY(-50%);transform: translateY(-50%); font-size: 1.8vw", 
                               "MODalyseR is a software for disease module-based analysis, which integrates information about regulatory mechanisms within modules to identify a core set of genes with strong disease association.",
                               style="font-family:Quicksand")),
             tags$div(`class`="col-sm-8", style = "height:100%; top: 25%;",
                      img(src = "www/MODalyseR_workflow.png", height = 600, width = 800))
             ),
    
    ## section 2
    tags$div(id="section2", style="height: 100vh; width: 100%;text-align:center; background-color:#0C090A", class= "row",
             tags$a(class="grid-item", style="background-image:url('www/grid_item1.jpg');",
                    tags$p("MODifieR", style="font-family:Quicksand"),
                    tags$p("MODifieR is an acronym for Module Identification and contains 8 different algorithms for identification of disease modules from transcriptome data.",
                           style="font-family:Quicksand; opacity:0; padding:7vh 1%; font-size:1vw;")),
             
             tags$a(class="grid-item", style="background-image:url('www/grid_item3.jpg');",
                    tags$p("ComHub", style="font-family:Quicksand"),
                    tags$p("ComHub is a tool for hub inference from transcriptomics data. Robust hub predictions are made by averaging over the predictions by a compendium of network inference methods. In total 7 network inference methods are included.",
                           style="font-family:Quicksand; opacity:0; padding:7vh 1%; font-size:1vw;")),
             
             tags$a(class="grid-item", style="background-image:url('www/grid_item2.jpg');",
                    tags$p("Enrichment analysis", style="font-family:Quicksand"),
                    tags$p("We provide enrichment analysis using the clusterprofiler package.",
                           tags$br(),
                           tags$br(),
                           tags$b("Available methods: "),
                           tags$br(),
                           tags$b("Disease analysis:"), "DOSE, DisGenNET and NCG repositories available",
                           tags$br(),
                           tags$br(),
                           tags$b("Gene ontology:"), "GO bioconductor database available",
                           tags$br(),
                           tags$br(),
                           tags$b("Pathway analysis:"), "KEGG database available",
                           style="font-family:Quicksand; opacity:0; padding:2vh 1%; font-size:1vw;")),
             
             tags$a(class="grid-item", style="background-image:url('www/grid_item3.jpg');",
                    tags$p("Cytoscape", style="font-family:Quicksand"),
                    tags$p("Cytoscape is a platform for visualizing networks. In MODalyseR you can start Cytoscape with selected results directly imported to Cytoscape, providing an easy solution to visualize modules.",
                           style="font-family:Quicksand; opacity:0; padding: 7vh 1%; font-size: 1vw;"))
             ),
    

    tags$hr(style="position:absolute; border:1px solid #3b3d4020; width:80vw; left:20vh; margin:0")
  )
}

#' welcoming_page Server Function
#'
#' @noRd 
mod_welcoming_page_server <- function(input, output, session){
  ns <- session$ns
  
}

## To be copied in the UI
# mod_welcoming_page_ui("welcoming_page_ui_1")

## To be copied in the server
# callModule(mod_welcoming_page_server, "welcoming_page_ui_1")
