#' Columns UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd 
#'
#' @importFrom shiny NS tagList 
mod_Columns_ui <- function(id){
  ns <- NS(id)
  tagList(
    # Number container
    tags$div(`class`="row", style = "margin-right:-15px; margin-left:-15px;",
             tags$div(`class`="col-sm-4", style = "-webkit-animation: fadein 1s; -moz-animation: fadein 1s; -ms-animation: fadein 1s;-o-animation: fadein 1s; animation: fadein 1s;",
                      tags$form(class = "well", style = "background-color: inherit",
                                tags$h2(class = "text-center",
                                        tags$span(
                                          class="label", "1. Upload data",
                                          style = "border-radius:25px;background-color:#ffbd40")))),
             
             tags$div(`class`="col-sm-4", style = "-webkit-animation: fadein 1s; -moz-animation: fadein 1s; -ms-animation: fadein 1s;-o-animation: fadein 1s; animation: fadein 1s;",
                      tags$form(class = "well", style = "background-color: inherit",
                                id = "num_contain_2",
                                tags$h2(class = "text-center",
                                        tags$span(
                                          #href = "javascript:void(0)",
                                          #onclick = "col2();",
                                          class="label", "2. Inference tools",
                                          #`data-hint`="If you click here you can skip directly to this column!",
                                          #`data-hintPosition`="top-right",
                                          style = "border-radius:25px;background-color:#ffbd40;")))),
             
             tags$div(`class`="col-sm-4", style = "-webkit-animation: fadein 1s; -moz-animation: fadein 1s; -ms-animation: fadein 1s;-o-animation: fadein 1s; animation: fadein 1s;",
                      tags$form(class = "well", style = "background-color: inherit",
                                id = "num_contain_3",
                                tags$h2(class = "text-center",
                                        tags$span(
                                          #href = "javascript:void(0)",
                                          #onclick = "col3();",
                                          class="label", "3. Analyse predictions",
                                          style = "border-radius:25px;background-color:#ffbd40;")))),
             
             #HTML("<dash id='arrow1' class='no1' style='left: 20.6vw;top: 60px;'></dash><dash id='arrow2' class='no1' style='left: 55vw;top: 60px;'></dash>"),
             #tags$i(class = "fa fa-chevron-right", style="position: relative;left: 46.5vw;bottom: 76.01px;"),
             #tags$i(class = "fa fa-chevron-right", style="position: relative;left: 80vw;bottom: 74px;")
    ),
    
    # Module container         
    tags$div(`class`="row",
             tags$div(`class`="col-sm-4", style = "-webkit-animation: fadein 1s; -moz-animation: fadein 1s; -ms-animation: fadein 1s;-o-animation: fadein 1s; animation: fadein 1s;",
                      tags$form(class = "well",
                                uiOutput(ns("input_choice")),
                                mod_upload_ui(ns("upload_ui_1"))),
                      tags$form(class = "well",
                                uiOutput(ns("comhub_input_choice")),
                                mod_init_comhub_ui(ns("init_comhub_ui_1"))),
                      tags$form(class = "well",
                                uiOutput(ns("nf_ppi")),
                                mod_upload_ppi_ui(ns("upload_ppi_ui_1"))),
                      tags$form(class = "well",
                                uiOutput(ns("nf_result_pc")),
                                mod_upload_nf_result_ui(ns("upload_nf_result_ui_1")))
                      ),
             
             tags$div(`class`="col-sm-4", style = "-webkit-animation: fadein 1s; -moz-animation: fadein 1s; -ms-animation: fadein 1s;-o-animation: fadein 1s; animation: fadein 1s;", 
                      id = "mod2",
                      tags$form(class = "well",
                                uiOutput(ns("module_inference_choice")),
                                mod_Description1_ui(ns("Description1_ui_1"))),
                      tags$form(class = "well",
                                uiOutput(ns("network_inference_choice")),
                                mod_network_inference_ui(ns("network_inference_ui_1"))),
                      tags$form(class = "well",
                                uiOutput(ns("comhub_choice")),
                                mod_comhub_ui(ns("comhub_ui_1"))),
                      tags$form(class = "well",
                                uiOutput(ns("comhub_modifier_choice")),
                                mod_modifier_comhub_ui(ns("modifier_comhub_ui_1"))),
                      ),
             
             
             tags$div(`class`="col-sm-4", style = "-webkit-animation: fadein 1s; -moz-animation: fadein 1s; -ms-animation: fadein 1s;-o-animation: fadein 1s; animation: fadein 1s;",
                      id = "mod3",
                      tags$form(class = "well",
                                uiOutput(ns("disease_analysis_choice")),
                                mod_disease_analysis_ui(ns("disease_analysis_ui_1"))),
                      tags$form(class = "well",
                                uiOutput(ns("label_genes")),
                                mod_label_genes_ui(ns("label_genes_ui_1"))),
                      tags$form(class = "well",
                                uiOutput(ns("cytoscape")),
                                mod_cytoscape_ui(ns("cytoscape_ui_1")))
                      )
             ),
    #htmlOutput(ns("fadein")),
    #htmlOutput(ns("fadein1")),
    rintrojs::introjsUI()
  )
  
}

#' Columns Server Function
#'
#' @noRd 
mod_Columns_server <- function(input, output, session, con, module_overview_ui_1, input_overview_ui_1,  ppi_networks_ui_1, comhub_overview_ui_1){
  ns <- session$ns
  
  Columns_module <- reactiveValues()
  
  upload_ui_1 <- callModule(mod_upload_server, "upload_ui_1", con = con)
  
  init_comhub_ui_1 <- callModule(mod_init_comhub_server, "init_comhub_ui_1", con = con, upload_ui_1, input_overview_ui_1, comhub_overview_ui_1)
  
  upload_ppi_ui_1 <- callModule(mod_upload_ppi_server, "upload_ppi_ui_1", con = con)
  
  upload_nf_result_ui_1 <- callModule(mod_upload_nf_result_server, "upload_nf_result_ui_1", con = con, upload_ui_1, input_overview_ui_1, module_overview_ui_1, comhub_overview_ui_1 )
  
  Description1_ui_1 <- callModule(mod_Description1_server, "Description1_ui_1", con = con, upload_ui_1, input_overview_ui_1, ppi_networks_ui_1)
  
  network_inference_ui_1 <- callModule(mod_network_inference_server, "network_inference_ui_1", con = con, init_comhub_ui_1, comhub_overview_ui_1)
  
  comhub_ui_1 <- callModule(mod_comhub_server, "comhub_ui_1", con = con, init_comhub_ui_1, comhub_overview_ui_1, network_inference_ui_1)
  
  modifier_comhub_ui_1 <- callModule(mod_modifier_comhub_server, "modifier_comhub_ui_1", con = con, upload_ui_1, input_overview_ui_1, init_comhub_ui_1)
  
  disease_analysis_ui_1 <- callModule(mod_disease_analysis_server, "disease_analysis_ui_1", con = con, Description1_ui_1, module_overview_ui_1)
  
  label_genes_ui_1 <- callModule(mod_label_genes_server, "label_genes_ui_1", con = con, Description1_ui_1, module_overview_ui_1)
  
  cytoscape_ui_1 <- callModule(mod_cytoscape_server, "cytoscape_ui_1", con = con)
  
  observeEvent(upload_ui_1$input_name,{
    Columns_module$input_name <- upload_ui_1$input_name
  })
  
  observeEvent(cytoscape_ui_1 $to_cy,{
    Columns_module$to_cy <- cytoscape_ui_1$to_cy
  })
  observeEvent(upload_ui_1$upload_input_rds,{
    Columns_module$upload_input_rds <- upload_ui_1$upload_input_rds
  }) #Is sent to input_overview for refreshing DT
  
  observeEvent(init_comhub_ui_1$upload,{
    Columns_module$comhub_upload <- init_comhub_ui_1$upload
  }) #Is sent to input_overview for refreshing DT
  
  observeEvent(upload_nf_result_ui_1$nf_name,{
    Columns_module$upload_nf_result <- upload_nf_result_ui_1$upload_nf_result
  })
  
  observeEvent(Description1_ui_1$infer,{
    Columns_module$infer <- Description1_ui_1$infer
  })
  
  observeEvent(network_inference_ui_1$infer,{
    Columns_module$infer_net <- network_inference_ui_1$infer_net
  })
  
  observeEvent(comhub_ui_1$infer,{
    Columns_module$infer_hub <- comhub_ui_1$infer_hub
  })
  
  observeEvent(disease_analysis_ui_1$enrich, {
    Columns_module$enrich <- disease_analysis_ui_1$enrich
  })
  #####
  observeEvent(upload_nf_result_ui_1$upload, {
    Columns_module$infer <- upload_nf_result_ui_1$upload
  })
  observeEvent(upload_ppi_ui_1$upload, {
     Columns_module$upload <- upload_ppi_ui_1$upload
  })
  #####
  # Activate column 2 and 3
  # observeEvent(upload_ui_1$input_name, {
  #  output$fadein <- renderUI ({
  #  tagList(
  #    tags$script("col2();")
  #  )
  #  })
  #})
  
  #observeEvent(upload_ui_1$upload_input_rds, {
  #  output$fadein <- renderUI ({
  #    tagList(
  #      tags$script("col2();")
  #    )
  #  })
  #})
  
  #output$fadein1 <- renderUI({
  #  req(Description1_ui_1$module_name)
  #  tagList(
  #    tags$script("col3();")
  #  )
  #})
  
  return(Columns_module)
} # Closes server function

## To be copied in the UI
# mod_Columns_ui("Columns_ui_1")

## To be copied in the server
# callModule(mod_Columns_server, "Columns_ui_1")

